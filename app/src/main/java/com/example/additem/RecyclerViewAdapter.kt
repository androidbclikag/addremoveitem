package com.example.additem

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.scrollable_layout.view.*

class RecyclerViewAdapter(private var myDataSet: MutableList<ItemModel>, private val newItem:OnClickNewItem):
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>(){

    inner class MyViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView), View.OnClickListener  {
        private lateinit var model: ItemModel
        fun onBind() {

            model = myDataSet[adapterPosition]
            itemView.imageView.setImageResource(model.Images)
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            newItem.onCLick(adapterPosition)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.scrollable_layout, parent, false)
    )

    override fun getItemCount(): Int = myDataSet.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind()
    }



}