package com.example.additem

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.text.FieldPosition

class MainActivity : AppCompatActivity() {
    private var items: MutableList<ItemModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private fun init() {
        addItemButton.setOnClickListener { addItem() }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RecyclerViewAdapter(items, object : OnClickNewItem {
            override fun onCLick(position: Int) {
                removeItem(position)
            }

        })


    }


    private fun addItem() {
        val newItem = ItemModel(R.mipmap.icon_star)
        items.add(newItem)
        recyclerView.adapter!!.notifyItemInserted(0)
        recyclerView.scrollToPosition(0)


    }


    private fun removeItem(position: Int) {
        items.removeAt(position)
        recyclerView.adapter!!.notifyItemRemoved(0)

    }


}
